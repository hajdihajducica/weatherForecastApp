import {Injectable} from "angular2/core";
import {WEATHER_ITEMS} from "./weather.data";
import {WeatherItem} from "./weather-item";
import {Observable} from "rxjs/Observable";
import {Http} from "angular2/http";
import 'rxjs/Rx';
@Injectable()

export class WeatherService{
	
	constructor  (private _http: Http) {}

	getWeatherItems(){
		return WEATHER_ITEMS;
	}

	addWeatherItem(weatherItem: WeatherItem) {
		WEATHER_ITEMS.push(weatherItem);
	}

	deleteWeatherItem(cityName){
	for(var i=0; i<WEATHER_ITEMS.length;i++){
	if(WEATHER_ITEMS[i]["cityName"] == cityName){
		WEATHER_ITEMS.splice(i,1);
	}

	}
	}
	

	searchWeatherData(cityName: string): Observable<any>{
		return this._http.get('http://api.openweathermap.org/data/2.5/weather?q=' + cityName + '&APPID=7f8cc223bd8267b432c9637c8cb608de&units=metric')
		.map(response => response.json())
		.catch(error => {
			console.error(error);
			return Observable.throw(error.json())
		});
	}
}