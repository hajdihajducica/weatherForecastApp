import {Component, Input} from 'angular2/core';
import {WeatherItem} from "./weather-item";

@Component ({
	selector: 'weather-item',
	template: `
	<div class="wrapper">
	<article class="weather-element">
		<div class="col-1">
			<h3>{{ weatherItem.cityName }}</h3>
			<p>{{ weatherItem.day }}</p>
		</div>
		<div class="col-2">
			<h2 class="temperature">{{ weatherItem.temperature }} &deg;</h2>
			<p>{{ weatherItem.description }}</p>
		</div>
		<div class="col-3">
			<p class="humidity">{{ weatherItem.humidity }} &#37;</p>
			<p class="wind">{{ weatherItem.wind }} km/h</p>
			<span class="delete" (click)="onDelete(weatherItem.cityName)">x</span>
		</div>
		</article>
		</div>
		
		`,
		 styleUrls: ['src/css/weather-item.css'],
			styleUrls: ['src/css/app.css'],
})

export class WeatherItemComponent {
@Input('item') weatherItem: WeatherItem;


	constructor(private weatherService: WeatherService){}

	onDelete(cityName){
		this.weatherService.deleteWeatherItem(cityName);
	}

}