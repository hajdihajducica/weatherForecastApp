export class WeatherItem{

	constructor(
	public cityName: string,
	public day: string,
	public temperature: number, 
	public description: string, 
	public humidity: number, 
	public wind: number)
	{
}


}