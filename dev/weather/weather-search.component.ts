import {Component} from "angular2/core";
import {ControlGroup} from "angular2/common";
import {WeatherService} from "./weather.service";
import {WeatherItem} from "./weather-item";

@Component({
	selector: 'weather-search',
	template: `
	
		<section class="weather-search">
			<form (ngSubmit)="onSubmit(f)" #f="ngForm">
				<input ngControl="location" type="text" id="city" required >
				<button type="submit">Add City</button>
			</form>		
		</section>
		`,
		 styleUrls: ['src/css/app.css'],
})

export class WeatherSearchComponent {


	constructor (private weatherService: WeatherService){}

	onSubmit(form: ControlGroup) {
		this.weatherService.searchWeatherData(form.value.location)
		.subscribe(
		data =>{
		const weatherItem = new WeatherItem(data.name, data.day.dt_txt, data.main.temp, data.weather[0].description, data.main.humidity, data.wind.speed);

			this.weatherService.addWeatherItem(weatherItem);
		}
);
}

}